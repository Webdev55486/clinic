var validate = function () {
    var handlenewuser = function () {
         $('.new-user-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
                    username: {
	                    required: true
	                },
	                password: {
	                    required: true,
						minlength: 6
	                },
	                password_confirmation: {
	                    equalTo: "#register_password"
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.new-user-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.new-user-form').validate().form()) {
	                    $('.new-user-form').submit();
	                }
	                return false;
	            }
	        });
	}
    
    var handleneworganization = function () {
         $('.new-organization-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {

	                name: {
	                    required: true
	                },
					code: {
	                    required: true
	                },
                    description: {
	                    required: true
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.new-organization-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.new-organization-form').validate().form()) {
	                    $('.new-organization-form').submit();
	                }
	                return false;
	            }
	        });
	}

    var handleedituser = function () {
         $('.edit-user-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
                    _username: {
	                    required: true
	                },
	                _password: {
						minlength: 6
	                },
	                _password_confirmation: {
	                    equalTo: "#_register_password"
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.edit-user-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.edit-user-form').validate().form()) {
	                    $('.edit-user-form').submit();
	                }
	                return false;
	            }
	        });
	}

    return {
        //main function to initiate the module
        init: function () {
            handlenewuser();
            handleedituser();
            handleneworganization();
        }
    };
}();

jQuery(document).ready(function() {
    validate.init();
});
