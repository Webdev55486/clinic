$(document).ready(function() {
    var callback_count = 0;
    var origine_value = null;
    $('#client-manage-table input').on('click', function(e) {
        e.preventDefault();
        var $this = $(this);
        $this.parent().css({'outline': '1px solid #b3b3b3', 'background-color': '#fff'});
        callback_count = 0;
        origine_value = $this.val();
    });
    
    $('#client-manage-table input').on('change', function(e) {
        e.preventDefault();
        var $that = $(this);
        $that.parent().css({'outline': '0', 'background-color': 'unset'});
        if (origine_value != $that.val() && callback_count == 0) {
            callback_count = 1;
            var current_value = $that.val();
            var save_url = $that.data('save_url');
            var client_id = $that.parents('tr').find('input.client_id_for_excel').val();
            
            axios.post(save_url, {clientId:client_id, currentDate:current_value}).then(function (response) {
                console.log(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        }
    });
})
