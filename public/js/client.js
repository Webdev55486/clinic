var Client = function () {
    var addNewClient = function () {

         $('.add-new-client-form').validate({
	            errorElement: 'div', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: true, // do not focus the last invalid input
	            ignore: "",
	            rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    doa: {
                        required: true
                    },
                    dob: {
                        required: true
                    },
                    age: {
                        required: true
                    },
                    gender: {
                        required: true
                    },
	                ie: {
	                    required: true
	                },
					ref: {
	                    required: true
	                },
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
                        // console.log($(element).closest('.form-group'));
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
                    // console.log(label);
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

            $('#dob').on('change', function() {
                var birthdate = $(this).val();
                var array_date = birthdate.split("/");

                var dob = array_date[2]+array_date[0]+array_date[1];
                var year = Number(dob.substr(0, 4));
                var month = Number(dob.substr(4, 2)) - 1;
                var day = Number(dob.substr(6, 2));
                var today = new Date();
                var age = today.getFullYear() - year;

                $('#age').val(age);
            })

			$('.add-new-client-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.add-new-client-form').validate().form()) {
	                    $('.add-new-client-form').submit();
	                }
	                return false;
	            }
	        });
	}

    return {
        //main function to initiate the module
        init: function () {

            addNewClient();

        }
    };
}();

jQuery(document).ready(function() {
    Client.init();
});
