@extends('layouts.app')
@section('title')
User Management
@endsection
@section('page_title')
User Management Page
@endsection
@section('pagelevel_cssplugin')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        <button class="close" data-close="alert"></button>
                        <span>{{ session('status') }}</span>
                    </div>
                @endif
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> User Manage </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a class="btn sbold green" href="#modal-new-user" data-toggle="modal"> New User
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="clinic-user-table">
                            <thead>
                                <tr>
                                    <th style="display:none;">
                                        <input type="checkbox" class="group-checkable" data-set="#clinic-user-table .checkboxes" /> </th>
                                    <th> Photo </th>
                                    <th> Username </th>
                                    <th> First Name </th>
                                    <th> Last Name </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($users as $user): ?>
                                    @if ($user->id != Auth::user()->id)
                                        <tr class="odd gradeX" id="user-list-{{$user->id}}">
                                            <td style="vertical-align: middle;display:none;">
                                                <input type="checkbox" class="checkboxes" value="1" /> </td>
                                            <td style="vertical-align: middle;">
                                                <?php if (file_exists('assets/images/avatar/'.$user->avatar)): ?>
                                                    <a href="{{url('user/view-single/'.$user->id)}}"><img style="width:100%;min-width:100px;" src="{{asset('assets/images/avatar/'.$user->avatar)}}" /></a>
                                                <?php else: ?>
                                                    <a href="{{url('user/view-single/'.$user->id)}}"><img style="width:100%;min-width:100px;" src="{{asset('assets/images/avatar/nophoto.jpg')}}" /></a>
                                                <?php endif; ?>
                                            </td>
                                            <td style="vertical-align: middle;" class="bold"> {{$user->username}} </td>
                                            <td style="vertical-align: middle;"> {{$user->first_name}} </td>
                                            <td style="vertical-align: middle;"> {{$user->last_name}} </td>
                                            <td style="vertical-align: middle;text-align: center;">
                                                <a onclick="edit_user({{$user->id}})" class="btn btn-sm btn-default green" style="width:75px;margin: 5px;">Edit</a>
                                                <a type="button" onclick="delete_user({{$user->id}})" class="btn btn-sm btn-default red-flamingo" style="width:75px;margin: 5px;">Delete</a>
                                            </td>
                                        </tr>
                                    @endif
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    {{ Form::open(['route' => 'users.add','class' => 'new-user-form', 'method' => 'post']) }}
        <div id="modal-new-user" class="modal fade" tabindex="-1" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-center bold">Add New User</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('first_name', 'First Name') }}
                                    {{ Form::text('first_name', old('first_name'), ['class' => 'form-control', 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('last_name', 'Last Name') }}
                                    {{ Form::text('last_name', old('last_name'), ['class' => 'form-control', 'required']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('username', 'User Name') }}
                            {{ Form::text('username', old('username'), ['class' => 'form-control', 'required']) }}
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('password', 'Password') }}
                                    <input name="password" class="form-control" type="password" value="" id="register_password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('password_confirmation', 'Password Confirm') }}
                                    <input name="password_confirmation" class="form-control" type="password" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dafault" data-dismiss="modal">Close</button>
                        {!! Form::submit('Add', ['class' => 'btn btn-success']) !!}
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}
    {{ Form::open(['route' => 'users.edit','class' => 'edit-user-form', 'method' => 'post']) }}
        <div id="modal-edit-user" class="modal fade" tabindex="-1" data-backdrop="static">
            <input type="hidden" name="user_id" id="user_id" value="">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-center bold">Edit User</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('_first_name', 'First Name') }}
                                    {{ Form::text('_first_name', old('_first_name'), ['class' => 'form-control', 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('_last_name', 'Last Name') }}
                                    {{ Form::text('_last_name', old('_last_name'), ['class' => 'form-control', 'required']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('_username', 'User Name') }}
                            {{ Form::text('_username', old('_username'), ['class' => 'form-control', 'required']) }}
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('_password', 'Password') }}
                                    <input name="_password" class="form-control" type="password" value="" id="_register_password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('_password_confirmation', 'Password Confirm') }}
                                    <input name="_password_confirmation" class="form-control" type="password" value="" id="_password_confirmation">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dafault" data-dismiss="modal">Close</button>
                        {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}
@endsection
@section('pagelevel_script')
    <script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{asset('js/userManage.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        var BASEURL = "{{ url('/user') }}";
        function delete_user(id) {
            var delete_url = BASEURL +'/delete/'+id;

            swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete!",
              cancelButtonText: "No, cancel!",
              showLoaderOnConfirm: true,
              closeOnConfirm: false,
              closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: delete_url,
                        type: 'get',
                        success: function(result){
                            console.log(result);
                            $("#user-list-"+id).remove();
                            swal("Deleted!", "User has been deleted.", "success");
                        },
                        error: function(result){
                            console.log(error);
                        }
                    });
                } else {
                    swal("Cancelled", "User is safe :)", "error");
                }
            });
        }

        function edit_user(id) {
            var get_url = BASEURL +'/edit/'+id;
            $.ajax({
                url: get_url,
                type: 'get',
                success: function(result){
                    $('#modal-edit-user #user_id').val(result.id);
                    $('#modal-edit-user #_first_name').val(result.first_name);
                    $('#modal-edit-user #_last_name').val(result.last_name);
                    $('#modal-edit-user #_username').val(result.username);
                    $('#modal-edit-user').modal('show');
                    // console.log(result.first_name);
                },
                error: function(error){
                    console.log(error);
                }
            });
        }
    </script>
@endsection
