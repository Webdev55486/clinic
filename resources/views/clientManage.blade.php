@extends('layouts.app')
@section('title')
Clients Management
@endsection
@section('page_title')
Clients Management
@endsection
@section('back_btn')
    <?php
        $setting = "";
        $own_setting_count = \App\ClientSetting::where('user_id', Auth::user()->id)->count();
        if ($own_setting_count > 0) {
            $setting = \App\ClientSetting::where('user_id', Auth::user()->id)->first();
        }
        else {
            $setting = \App\ClientSettingAdmin::first();
        }
    ?>
    <div class="page-toolbar" style="margin: 15px 0;">
        <div class="btn-group pull-right">
            <button type="button" id="width-control-btn" class="btn green dropdown-toggle btn-sm" data-toggle="dropdown"> Reset Width <i class="fa fa-angle-down"></i></button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="width-control-btn">
                <li @if ($setting->width == 0) class="active" @endif>
                    <a href="javascript: width_boxed();"> <i class="fa fa-compress"></i> Boxed </a>
                </li>
                <li @if ($setting->width == 1) class="active" @endif>
                    <a href="javascript: width_fluid();"> <i class="fa fa-expand"></i> Fluid </a>
                </li>
            </ul>
        </div>
    </div>
@endsection
@section('pagelevel_cssplugin')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/icheck/skins/all.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if (session('error'))
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        <button class="close" data-close="alert"></button>
                        <span>{{ session('status') }}</span>
                    </div>
                @endif
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Client Manage </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a id="sample_editable_1_new" href="#modal-add-new-client" data-toggle="modal" class="btn sbold green"> Add New
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group pull-right">
                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="#modal-edit-table-cell" data-toggle="modal"><i class="icon-settings"></i> Customize TableCell </a>
                                            </li>
                                            <li>
                                                <a href="{{url('client/save-pdf/'.Auth::user()->organization_id)}}" target="_blank">
                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                            </li>
                                            <li>
                                                <a href="{{url('client/save-xls/'.Auth::user()->organization_id)}}" target="_blank">
                                                    <i class="fa fa-file-pdf-o"></i> Save as xls </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="client-manage-table">
                            <thead>
                                <tr>
                                    <th style="display:none;">
                                        <input type="checkbox" class="group-checkable" data-set="#client-manage-table .checkboxes" /> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Client Name">Name</label> </th>
                                    @if ($setting->age == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Age">Age</label> </th>
                                    @endif
                                    @if ($setting->gender == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Gender">Gender</label> </th>
                                    @endif
                                    @if ($setting->dob == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of Birth">DOB</label> </th>
                                    @endif
                                    @if ($setting->doa == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of Accident">DOA</label> </th>
                                    @endif
                                    @if ($setting->ie == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of Initial Evaluation">IE</label> </th>
                                    @endif
                                    @if ($setting->ddv == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of last Daily Visit">DDV</label> </th>
                                    @endif
                                    @if ($setting->ndv == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Number of Daily Visits">NDV</label> </th>
                                    @endif
                                    @if ($setting->re == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of most last Re-evaluation">RE</label> </th>
                                    @endif
                                    @if ($setting->nre == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next Re-evaluation">NRE</label> </th>
                                    @endif
                                    @if ($setting->ref == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of Most last Referral">REF</label> </th>
                                    @endif
                                    @if ($setting->nref == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next Referral">NREF</label> </th>
                                    @endif
                                    @if ($setting->rom == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of last Range of motion Test">ROM</label> </th>
                                    @endif
                                    @if ($setting->nrom == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next Range of Motion test">NROM</label> </th>
                                    @endif
                                    @if ($setting->mmt_u == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of last manual muscle test - upper">MMT-U</label> </th>
                                    @endif
                                    @if ($setting->nmmt_u == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next manual muscle test - upper">NMMT-U</label> </th>
                                    @endif
                                    @if ($setting->mmt_l == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of last manual muscle test - lower">MMT-L</label> </th>
                                    @endif
                                    @if ($setting->nmmt_l == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next manual muscle test - lower">NMMT-L</label> </th>
                                    @endif
                                    @if ($setting->fct == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of last Functional Capacity test ">FCT</label> </th>
                                    @endif
                                    @if ($setting->nfct == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next Functional Capacity test">NFCT</label> </th>
                                    @endif
                                    @if ($setting->d_d_c == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of Discharge">D-D/C</label> </th>
                                    @endif
                                    @if ($setting->d_c == 1)
                                        <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of Discharge">D/C</label> </th>
                                    @endif
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    function change_dateformat($date) {
                                        if ($date) {
                                            $resultdate = DateTime::createFromFormat('Y-m-d', $date);
                                            $final_date = $resultdate->format('m/d/Y');
                                            return $final_date;
                                        }
                                        else {
                                            return "NULL";
                                        }
                                    }
                                ?>
                                @foreach ($clients as $client)
                                    <tr id="client_tr_{{$client->id}}">
                                        <input type="hidden" class="client_id_for_excel" value="{{$client->id}}">
                                        <td style="display:none;">
                                            <input type="checkbox" class="checkboxes" value="1" /> </td>
                                        <td> <a href="javascript: show_edit_client({{$client->id}})" style="white-space:nowrap;">{{$client->first_name}} {{$client->last_name}}</a> </td>
                                        @if ($setting->age == 1)
                                            <td> {{$client->age}} </td>
                                        @endif
                                        @if ($setting->gender == 1)
                                            <td>
                                                @if ($client->gender == 1)
                                                    Male
                                                @else
                                                    Female
                                                @endif
                                            </td>
                                        @endif
                                        @if ($setting->dob == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/dob')}}" value="{{change_dateformat($client->dob)}}"></td>
                                        @endif
                                        @if ($setting->doa == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/doa')}}" value="{{change_dateformat($client->doa)}}"></td>
                                        @endif
                                        @if ($setting->ie == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/ie')}}" value="{{change_dateformat($client->ie)}}"></td>
                                        @endif
                                        @if ($setting->ddv == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/ddv')}}" value="{{change_dateformat($client->ddv)}}"></td>
                                        @endif
                                        @if ($setting->ndv == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/ndv')}}" value="{{change_dateformat($client->ndv)}}"></td>
                                        @endif
                                        @if ($setting->re == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/re')}}" value="{{change_dateformat($client->re)}}"></td>
                                        @endif
                                        @if ($setting->nre == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/nre')}}" value="{{change_dateformat($client->nre)}}"></td>
                                        @endif
                                        @if ($setting->ref == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/ref')}}" value="{{change_dateformat($client->ref)}}"></td>
                                        @endif
                                        @if ($setting->nref == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/nref')}}" value="{{change_dateformat($client->nref)}}"></td>
                                        @endif
                                        @if ($setting->rom == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/rom')}}" value="{{change_dateformat($client->rom)}}"></td>
                                        @endif
                                        @if ($setting->nrom == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/nrom')}}" value="{{change_dateformat($client->nrom)}}"></td>
                                        @endif
                                        @if ($setting->mmt_u == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/mmt_u')}}" value="{{change_dateformat($client->mmt_u)}}"></td>
                                        @endif
                                        @if ($setting->nmmt_u == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/nmmt_u')}}" value="{{change_dateformat($client->nmmt_u)}}"></td>
                                        @endif
                                        @if ($setting->mmt_l == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/mmt_l')}}" value="{{change_dateformat($client->mmt_l)}}"></td>
                                        @endif
                                        @if ($setting->nmmt_l == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/nmmt_l')}}" value="{{change_dateformat($client->nmmt_l)}}"></td>
                                        @endif
                                        @if ($setting->fct == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/fct')}}" value="{{change_dateformat($client->fct)}}"></td>
                                        @endif
                                        @if ($setting->nfct == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/nfct')}}" value="{{change_dateformat($client->nfct)}}"></td>
                                        @endif
                                        @if ($setting->d_d_c == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/d_d_c')}}" value="{{change_dateformat($client->d_d_c)}}"></td>
                                        @endif
                                        @if ($setting->d_c == 1)
                                            <td><input type="text" class="date-picker" data-save_url="{{url('client/update/d_c')}}" value="{{change_dateformat($client->d_c)}}"></td>
                                        @endif
                                        <td style="text-align:center;"><a type="button" onclick="delete_client({{$client->id}})" class="btn btn-sm btn-default red-flamingo" style="width:75px;margin: 5px;">Delete</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>

    <div id="modal-edit-table-cell" class="modal fade" data-backdrop="static" tabindex="-1" data-width="500">
        <form action="{{route('client.tablesetting')}}" class="edit-table-cell-form" method="post">
            {{ csrf_field() }}
            <div class="modal-header">
                <h4 class="text-center bold">Table Cell Customize</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->age == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="age"> Age </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->gender == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="gender"> Gender </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->doa == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="doa"> Date of Accident (DOA) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->dob == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="dob"> Date of Birth (DOB) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->ie == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="ie"> Date of Initial Evaluation (IE) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->ddv == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="ddv"> Date of last Daily Visit (DDV) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->ndv == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="ndv"> Number of Daily Visits (NDV) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->re == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="re"> Date of most last Re-evaluation (RE) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->nre == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="nre"> Due date of next Re-evaluation (NRE) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->ref == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="ref"> Date of Most last Referral (REF) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->nref == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="nref"> Due date of next Referral (NREF) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->rom == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="rom"> Date of last Range of motion Test (ROM) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->nrom == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="nrom"> Due date of next Range of Motion test (NROM) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->mmt_u == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="mmt_u"> Date of last manual muscle test - upper (MMT-U) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->nmmt_u == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="nmmt_u"> Due date of next manual muscle test - upper (NMMT-U) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->mmt_l == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="mmt_l"> Date of last manual muscle test - lower (MMT-L) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->nmmt_l == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="nmmt_l"> Due date of next manual muscle test - lower (NMMT-L) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->fct == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="fct"> Date of last Functional Capacity test (FCT) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->nfct == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="nfct"> Due date of next Functional Capacity test (NFCT) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->d_d_c == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="d_d_c"> Due date of Discharge (D-D/C) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if ($setting->d_c == 1) checked @endif data-checkbox="icheckbox_minimal" name="table_setting[]" value="d_c"> Date of Discharge (D/C) </label>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" id="client_table_custom_check" class="icheck" data-checkbox="icheckbox_minimal"> Check All </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dafault" data-dismiss="modal">Cancel</button>
                {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
            </div>
        </form>
    </div>

    <div id="modal-add-new-client" class="modal fade" data-backdrop="static" tabindex="-1" data-width="500">
        <form action="{{route('client.store')}}" class="add-new-client-form" method="post">
            {{ csrf_field() }}
            <div class="modal-header">
                <h4 class="text-center bold">Add New Client</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('first_name', 'First Name') }}
                            {{ Form::text('first_name', old('first_name'), ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('last_name', 'Last Name') }}
                            {{ Form::text('last_name', old('last_name'), ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('doa', 'Date of Accident') }}
                            {{ Form::text('doa', old('doa'), ['class' => 'form-control date-picker', 'required']) }}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('dob', 'Date of Birth') }}
                            {{ Form::text('dob', old('dob'), ['class' => 'form-control date-picker', 'required']) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <input type="hidden" name="age" id="age" value="">
                        <div class="form-group">
                            <label class="control-label">Organization</label>
                            <select name="organization" id="organization" class="form-control" disabled>
                                @foreach ($organizations as $organization)
                                    <option value="{{$organization->id}}" @if ($organization->id == Auth::user()->organization_id) selected @endif>{{$organization->name}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="organization" value="{{Auth::user()->organization_id}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Gender</label>
                            <select name="gender" class="form-control">
                                <option value="1" selected>Male</option>
                                <option value="0">Female</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('ie', 'Date of Initial Evaluation') }}
                            {{ Form::text('ie', old('ie'), ['class' => 'form-control date-picker', 'required']) }}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('ref', 'Date of Referral') }}
                            {{ Form::text('ref', old('ref'), ['class' => 'form-control date-picker', 'required']) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dafault" data-dismiss="modal">Cancel</button>
                {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
            </div>
        </form>
    </div>

    <div id="modal-edit-client" class="modal fade" data-backdrop="static" tabindex="-1" data-width="500">
        <form action="{{route('client.edit')}}" class="edit-new-client-form" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="client_id" id="client_id" value="">
            <div class="modal-header">
                <h4 class="text-center bold">Add Info To <span class="client_name font-red-flamingo"></span> </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('today', 'Today Date') }}
                            {{ Form::text('today', old('today'), ['class' => 'form-control date-picker', 'required']) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="bold" style="margin-top:0;">choose any of these services</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="client_info[]" value="ddv"> Daily Visit (DDV) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="client_info[]" value="re"> Re-evaluation (RE) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="client_info[]" value="ref"> Referral (REF) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="client_info[]" value="rom"> Range of motion Test (ROM) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="client_info[]" value="mmt_u"> Manual muscle test - Upper (MMT-U) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="client_info[]" value="mmt_l"> Manual muscle test - Lower (MMT-L) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="client_info[]" value="fct"> Functional Capacity test (FCT) </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="client_info[]" value="d_c"> Discharge (D/C) </label>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-12">
                        <label style="cursor:pointer;" class="bold"><input type="checkbox" id="client_add_info_all_check" class="icheck" data-checkbox="icheckbox_minimal"> Check All </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dafault" data-dismiss="modal">Cancel</button>
                {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
            </div>
        </form>
    </div>
@endsection
@section('pagelevel_script')
    <script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/form-icheck.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.js')}}" type="text/javascript"></script>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/icheck/icheck.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{ asset('js/client.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/excel.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        var width_set = {{$setting->width}};
        var BASEURL = "{{ url('/client') }}";
        var today_date = new Date();
        var dd = today_date.getDate();
        var mm = today_date.getMonth()+1; //January is 0!
        var yyyy = today_date.getFullYear();

        if(dd<10) {
            dd = '0'+dd
        }

        if(mm<10) {
            mm = '0'+mm
        }

        today_date = mm + '/' + dd + '/' + yyyy;

        $(document).ready(function(){
            if (width_set == 1) {
                $('body').find('div.container').each(function(){
                    $(this).removeClass('container');
                    $(this).addClass('container-fluid');
                });
            }
            else if (width_set == 0) {
                $('body').find('div.container-fluid').each(function(){
                    $(this).removeClass('container-fluid');
                    $(this).addClass('container');
                });
            }

            $('#client_add_info_all_check').on('ifChecked', function(event){
                $('#modal-edit-client').find('input.icheck').each(function() {
                    $(this).iCheck('check');
                });
            });

            $('#client_add_info_all_check').on('ifUnchecked', function(event){
                $('#modal-edit-client').find('input.icheck').each(function() {
                    $(this).iCheck('uncheck');
                });
            });

            $('#client_table_custom_check').on('ifChecked', function(event){
                $('#modal-edit-table-cell').find('input.icheck').each(function() {
                    $(this).iCheck('check');
                });
            });

            $('#client_table_custom_check').on('ifUnchecked', function(event){
                $('#modal-edit-table-cell').find('input.icheck').each(function() {
                    $(this).iCheck('uncheck');
                });
            });
        });
        function width_fluid() {
            $('body').find('div.container').each(function(){
                $(this).removeClass('container');
                $(this).addClass('container-fluid');
            });

            set_widthstatus(1);
        }

        function width_boxed() {
            $('body').find('div.container-fluid').each(function(){
                $(this).removeClass('container-fluid');
                $(this).addClass('container');
            });

            set_widthstatus(0);
        }

        function set_widthstatus(width_value) {
            var get_url = BASEURL +'/get-width/'+width_value;
            $.ajax({
                url: get_url,
                type: 'get',
                success: function(result){
                    console.log(result);
                },
                error: function(error){
                    console.log(error);
                }
            });
        }

        function show_edit_client(id) {
            var get_url = BASEURL +'/get-single/'+id;
            $.ajax({
                url: get_url,
                type: 'get',
                success: function(result){
                    $('#modal-edit-client #client_id').val(result.id);
                    $('#modal-edit-client .client_name').html(result.first_name+" "+result.last_name);
                    $('#modal-edit-client #today').val(today_date);

                    $('#modal-edit-client').find('input.icheck').each(function() {
                        $(this).iCheck('uncheck');
                    });
                    $('#modal-edit-client').modal("show");
                    console.log(result);
                },
                error: function(error){
                    console.log(error);
                }
            });
        }
        
        function delete_client(id) {
            var default_delete_url = "{{url('client/delete')}}";
            var final_delete_url = default_delete_url+"/"+id;
            
            swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete!",
              cancelButtonText: "No, cancel!",
              showLoaderOnConfirm: true,
              closeOnConfirm: false,
              closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: final_delete_url,
                        type: 'get',
                        success: function(result){
                            console.log(result);
                            if (result == "success") {
                                $('#client_tr_'+id).remove();
                                swal({
                                    title: "Success!",
                                    text: "Client Deleted",
                                    timer: 1000,
                                    type: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false
                                });
                            }
                            else {
                                swal({
                                    title: "Failed!",
                                    text: "Went something wrong",
                                    timer: 1000,
                                    type: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false
                                });
                            }
                        },
                        error: function(result){
                            console.log(error);
                        }
                    });
                } else {
                    console.log("canceled");
                }
            });
        }
    </script>
@endsection
