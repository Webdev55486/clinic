@extends('auth.master')
@section('title')
Register
@endsection
@section('content')
    <!-- BEGIN REGISTRATION FORM -->
    <form class="register-form" action="{{ route('register')}}" method="post">
        <h3 class="form-title text-center bold uppercase">Sign Up to {{$organization->name}}</h3>
        <input type="hidden" name="organization_id" value="{{$organization->id}}">
        @if (session('error'))
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                {{ session('error') }}
            </div>
        @endif
        {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">First Name</label>
            <div class="input-icon">
                <i class="fa fa-font"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="first_name" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Last Name</label>
            <div class="input-icon">
                <i class="fa fa-font"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="last_name" />
            </div>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">User Name</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Username" name="username" /> </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" /> </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
            <div class="controls">
                <div class="input-icon">
                    <i class="fa fa-check"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="password_confirmation" /> </div>
            </div>
        </div>
        <div class="form-actions">
            <a href="{{route('login')}}" class="btn red btn-outline"> Back </a>
            <button type="submit" id="register-submit-btn" class="btn green pull-right"> Sign Up </button>
        </div>
    </form>
    <!-- END REGISTRATION FORM -->
@endsection
