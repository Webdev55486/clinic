@extends('auth.master')
@section('title')
Admin login
@endsection
@section('content')
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{ route('admin.login')}}" method="post">
        <h3 class="form-title text-center bold uppercase">Admin Login</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Enter Username and password. </span>
        </div>
        @if (session('error'))
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                {{ session('error') }}
            </div>
        @endif
        {{ csrf_field() }}
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Name</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="User Name" name="name" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />
            </div>
        </div>
        <div class="form-actions">
            <label class="checkbox" style="cursor:pointer;">
                <input type="checkbox" name="remember" value="1" /> Remember me </label>
            <button type="submit" class="btn green pull-right"> Login </button>
        </div>
        <div class="create-account">
            <p>If you are not Admin &nbsp; <a style="color: #fbfbfb;text-shadow: 0 0 3px #fff;" href="{{route('organization')}}"> Login as User </a></p>
        </div>
    </form>
    <!-- END LOGIN FORM -->
@endsection
