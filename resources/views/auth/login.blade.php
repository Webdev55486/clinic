@extends('auth.master')
@section('title')
login
@endsection
@section('content')
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{ route('login')}}" method="post">
        <h3 class="form-title text-center bold uppercase">Login to {{$organization->name}}</h3>
        <input type="hidden" name="organization_id" value="{{$organization->id}}">
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Enter Username and password. </span>
        </div>
        @if (session('error'))
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                {{ session('error') }}
            </div>
        @endif
        {{ csrf_field() }}
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Name</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="User Name" name="username" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />
            </div>
        </div>
        <div class="form-actions">
            <label class="checkbox" style="cursor:pointer;">
                <input type="checkbox" name="remember" value="1" /> Remember me </label>
            <button type="submit" class="btn green pull-right"> Login </button>
            {{-- <div class="forget-password">
                <h4>Forgot your password ?</h4>
                <p> No worries, click <a> here </a> to reset your password. </p>
            </div> --}}
        </div>
    </form>
    <div class="create-account">
        <p> Don't have an account yet ?&nbsp;
            <a href="{{ route('register.org') }}" onclick="event.preventDefault(); document.getElementById('register_form').submit();">
                Create an account
            </a>
        </p>
        <form id="register_form" action="{{ route('register.org') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="organization_id" value="{{$organization->id}}">
        </form>
    </div>
    <!-- END LOGIN FORM -->
@endsection
