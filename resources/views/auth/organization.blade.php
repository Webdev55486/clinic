@extends('auth.master')
@section('title')
Organization Code
@endsection
@section('content')
    <!-- BEGIN LOGIN FORM -->
    <form class="organization-form" action="{{ route('login.org')}}" method="post">
        <h3 class="form-title text-center bold">Enter organization code and click run.</h3>
        @if (session('error'))
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                {{ session('error') }}
            </div>
        @endif
        {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Your Organization code" name="organication" />
            </div>
        </div>
        <div class="form-actions text-center">
            <button type="submit" class="btn green"> Run </button>
        </div>
        <div class="create-account">
            <p> Are you a Admin ?&nbsp;
                <a style="color: #fbfbfb;text-shadow: 0 0 3px #fff;" href="{{route('admin.login')}}"> Login as Admin </a>
            </p>
        </div>
    </form>
    <!-- END LOGIN FORM -->
@endsection
