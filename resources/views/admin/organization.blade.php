@extends('layouts.adminApp')
@section('title')
Organization Management
@endsection
@section('page_title')
Organization Management Page
@endsection
@section('pagelevel_cssplugin')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        <button class="close" data-close="alert"></button>
                        <span>{{ session('status') }}</span>
                    </div>
                @endif
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Organization Manage </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a class="btn sbold green" href="#modal-new-organization" data-toggle="modal"> New Organization
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="clinic-organization-table">
                            <thead>
                                <tr>
                                    <th style="display:none;">
                                        <input type="checkbox" class="group-checkable" data-set="#clinic-organization-table .checkboxes" /> </th>
                                    <th> Name </th>
                                    <th> Code </th>
                                    <th> Description </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($organizations as $organization): ?>
                                    <tr class="odd gradeX" id="organiation-list-{{$organization->id}}">
                                        <td style="vertical-align: middle;display:none;">
                                            <input type="checkbox" class="checkboxes" value="1" /> </td>
                                        <td style="vertical-align: middle;" class="bold"> {{$organization->name}} </td>
                                        <td style="vertical-align: middle;"> {{$organization->code}} </td>
                                        <td style="vertical-align: middle;"> {{$organization->description}} </td>
                                        <td style="vertical-align: middle;text-align: center;">
                                            <a onclick="edit_organization({{$organization->id}})" class="btn btn-sm btn-default green" style="width:75px;margin: 5px;">Edit</a>
                                            <a type="button" onclick="delete_organization({{$organization->id}})" class="btn btn-sm btn-default red-flamingo" style="width:75px;margin: 5px;">Delete</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    
    {{ Form::open(['route' => 'admin.organization.store','class' => 'new-organization-form', 'method' => 'post']) }}
        <div id="modal-new-organization" class="modal fade" tabindex="-1" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-center bold">Add New Organization</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', old('name'), ['class' => 'form-control', 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('code', 'Code') }}
                                    {{ Form::text('code', old('code'), ['class' => 'form-control', 'required']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('description', 'Description') }}
                            <textarea name="description" class="form-control" placeholder="Description" rows="3" cols="80" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dafault" data-dismiss="modal">Close</button>
                        {!! Form::submit('Add', ['class' => 'btn btn-success']) !!}
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}
    
    {{ Form::open(['route' => 'admin.organization.update','class' => 'edit-organization-form', 'method' => 'post']) }}
        <div id="modal-edit-organization" class="modal fade" tabindex="-1" data-backdrop="static">
            <input type="hidden" name="organization_id" id="organization_id" value="">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-center bold">Edit Organization</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('_name', 'Name') }}
                                    {{ Form::text('_name', old('_name'), ['class' => 'form-control', 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('_code', 'Code') }}
                                    {{ Form::text('_code', old('_code'), ['class' => 'form-control', 'required']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('_description', 'Description') }}
                            <textarea name="_description" id="_description" class="form-control" placeholder="Description" rows="3" cols="80" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dafault" data-dismiss="modal">Close</button>
                        {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}
@endsection
@section('pagelevel_script')
    <script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{asset('js/userManage.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        var BASEURL = "{{ url('/admin/organization/') }}";
        function delete_organization(id) {
            var delete_url = BASEURL +'/delete/'+id;

            swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete!",
              cancelButtonText: "No, cancel!",
              showLoaderOnConfirm: true,
              closeOnConfirm: false,
              closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: delete_url,
                        type: 'get',
                        success: function(result){
                            if (result == "success") {
                                $("#organiation-list-"+id).remove();
                                swal({
                                    title: "Success!",
                                    text: "Organization Deleted",
                                    timer: 1000,
                                    type: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false
                                });
                            }
                            else {
                                swal({
                                    title: "Failed!",
                                    text: "Delete failed",
                                    timer: 1000,
                                    type: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false
                                });
                            }
                        },
                        error: function(result){
                            console.log(error);
                        }
                    });
                } else {
                    console.log('canceled');
                }
            });
        }

        function edit_organization(id) {
            var get_url = BASEURL +'/get/'+id;
            $.ajax({
                url: get_url,
                type: 'get',
                success: function(result){
                    if (result != "fail") {
                        $('#modal-edit-organization #organization_id').val(result.id);
                        $('#modal-edit-organization #_name').val(result.name);
                        $('#modal-edit-organization #_code').val(result.code);
                        $('#modal-edit-organization #_description').val(result.description);
                        $('#modal-edit-organization').modal('show');
                    }else {
                        console.log(result);
                    }
                },
                error: function(error){
                    console.log(error);
                }
            });
        }
    </script>
@endsection
