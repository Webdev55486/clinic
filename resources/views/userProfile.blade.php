@extends('layouts.app')
@section('title')
user Profile
@endsection
@section('page_title')
{{$user->username}}'s Profile
@endsection
@section('back_btn')
<div class="page-toolbar" style="margin: 15px 0;">
    <div class="btn-group pull-right">
        <a href="{{route('users.view')}}" class="btn green btn-sm"> Back To User List </a>
    </div>
</div>
@endsection
@section('pagelevel_cssplugin')
    <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('custom_style')
    <link href="{{asset('css/profile.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div style="width: 100%;position: relative;">
                    <div class="user-profile-container-div zoomIn">
                        <div class="card hovercard">
                            <div class="useravatar">
                                @if(file_exists('assets/images/avatar/'.'/'.$user->avatar))
                                  <img alt=""  src="{{ asset('assets/images/avatar/').'/'.$user->avatar}}" />
                                @else
                                  <img alt="" src="{{ asset('assets/images/avatar/nophoto.jpg') }}" />
                                @endif
                            </div>
                            <div class="card-info"> <span class="card-title">{{$user->username}} ( {{$user->first_name}} {{$user->last_name}} )</span></div>
                        </div>
                        <div class="tab-controller-container">
                            <div class="tab-container">
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                @endif
                                @if (session('error'))
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        {{ session('error') }}
                                    </div>
                                @endif
                                <div id="profile-bootstrap-alert"> </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab2">
                                        <div class="row">
                                            <div class="col-sm-4 col-sm-offset-4 text-center">
                                                <form action="{{route('user.edit.photo')}}" role="form" method="post" class="user-avartar-upload-form" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                                    <div class="slim" data-ratio="1:1">
                            	                        <input type="file" name="slim[]" required/>
                            	                    </div>
                                                    <div class="margin-top-10">
                                                        <button class="btn green" type="submit"> Save Changes </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{asset('js/profile.js')}}" type="text/javascript"></script>
@endsection
