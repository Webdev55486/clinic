<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Clinic | Clients</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ asset('assets/global/css/components-md.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('assets/global/css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ asset('assets/layouts/layout3/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/layouts/layout3/css/themes/default.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ asset('assets/layouts/layout3/css/custom.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <!-- BEGINE CUSTOM STYLES -->
        <!-- END CUSTOM STYLES -->
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}" />
    </head>
    <body class="page-container-bg-solid page-boxed">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin: 0;">
                            <thead>
                                <tr>
                                    <th style="display:none;"><input type="checkbox" class="group-checkable" data-set="#client-manage-table .checkboxes" /> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Client Name">Name</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Age">Age</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Gender">Gender</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of Birth">DOB</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of Accident">DOA</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of Initial Evaluation">IE</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of last Daily Visit">DDV</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Number of Daily Visits">NDV</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of most last Re-evaluation">RE</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next Re-evaluation">NRE</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of Most last Referral">REF</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next Referral">NREF</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of last Range of motion Test">ROM</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next Range of Motion test">NROM</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of last manual muscle test - upper">MMT-U</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next manual muscle test - upper">NMMT-U</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of last manual muscle test - lower">MMT-L</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next manual muscle test - lower">NMMT-L</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of last Functional Capacity test ">FCT</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of next Functional Capacity test">NFCT</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Due date of Discharge">D-D/C</label> </th>
                                    <th> <label style="width:100%;margin:0;cursor:pointer;" class="tooltips bold" data-placement="bottom" data-original-title="Date of Discharge">D/C</label> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    function change_dateformat($date) {
                                        if ($date) {
                                            $resultdate = DateTime::createFromFormat('Y-m-d', $date);
                                            $final_date = $resultdate->format('m/d/Y');
                                            return $final_date;
                                        }
                                        else {
                                            return "NULL";
                                        }
                                    }
                                ?>
                                @foreach ($clients as $client)
                                    <tr id="client_tr_{{$client->id}}">
                                        <input type="hidden" class="client_id_for_excel" value="{{$client->id}}">
                                        <td style="display:none;">
                                            <input type="checkbox" class="checkboxes" value="1" /> </td>
                                        <td>{{$client->first_name}} {{$client->last_name}}</td>
                                        <td contentEditable="true"> {{$client->age}} </td>
                                        <td contentEditable="true">
                                            @if ($client->gender == 1)
                                                Male
                                            @else
                                                Female
                                            @endif
                                        </td>
                                        <td>{{change_dateformat($client->dob)}}</td>
                                        <td>{{change_dateformat($client->doa)}}</td>
                                        <td>{{change_dateformat($client->ie)}}</td>
                                        <td>{{change_dateformat($client->ddv)}}</td>
                                        <td>{{change_dateformat($client->ndv)}}</td>
                                        <td>{{change_dateformat($client->re)}}</td>
                                        <td>{{change_dateformat($client->nre)}}</td>
                                        <td>{{change_dateformat($client->ref)}}</td>
                                        <td>{{change_dateformat($client->nref)}}</td>
                                        <td>{{change_dateformat($client->rom)}}</td>
                                        <td>{{change_dateformat($client->nrom)}}</td>
                                        <td>{{change_dateformat($client->mmt_u)}}</td>
                                        <td>{{change_dateformat($client->nmmt_u)}}</td>
                                        <td>{{change_dateformat($client->mmt_l)}}</td>
                                        <td>{{change_dateformat($client->nmmt_l)}}</td>
                                        <td>{{change_dateformat($client->fct)}}</td>
                                        <td>{{change_dateformat($client->nfct)}}</td>
                                        <td>{{change_dateformat($client->d_d_c)}}</td>
                                        <td>{{change_dateformat($client->d_c)}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ asset('assets/layouts/layout3/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/layout3/scripts/demo.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <!-- BEGINE CUSTOM SCRIPTS -->
        <!-- END CUSTOM SCRIPTS -->
    </body>

</html>
