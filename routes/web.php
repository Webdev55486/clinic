<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::post('/profile-edit-bio', 'HomeController@profile_edit_bio')->name('profile.edit.bio');
Route::post('/profile-edit-photo', 'HomeController@profile_edit_photo')->name('profile.edit.photo');
Route::post('/profile-edit-pass', 'HomeController@profile_edit_pass')->name('profile.edit.pass');

Route::prefix('client')->group(function(){
    Route::get('/view', 'ClientController@client')->name('client.view');
    Route::post('/table-setting', 'ClientController@custome_table')->name('client.tablesetting');
    Route::post('/store', 'ClientController@client_store')->name('client.store');
    Route::post('/update', 'ClientController@client_update')->name('client.edit');
    Route::get('/get-width/{id}', 'ClientController@get_setting_width');
    Route::get('/get-single/{id}', 'ClientController@get_single_client');
    Route::post('/update/dob', 'ClientController@client_update_dob');
    Route::post('/update/doa', 'ClientController@client_update_doa');
    Route::post('/update/ie', 'ClientController@client_update_ie');
    Route::post('/update/ddv', 'ClientController@client_update_ddv');
    Route::post('/update/ndv', 'ClientController@client_update_ndv');
    Route::post('/update/re', 'ClientController@client_update_re');
    Route::post('/update/nre', 'ClientController@client_update_nre');
    Route::post('/update/ref', 'ClientController@client_update_ref');
    Route::post('/update/nref', 'ClientController@client_update_nref');
    Route::post('/update/rom', 'ClientController@client_update_rom');
    Route::post('/update/nrom', 'ClientController@client_update_nrom');
    Route::post('/update/mmt_u', 'ClientController@client_update_mmt_u');
    Route::post('/update/nmmt_u', 'ClientController@client_update_nmmt_u');
    Route::post('/update/mmt_l', 'ClientController@client_update_mmt_l');
    Route::post('/update/nmmt_l', 'ClientController@client_update_nmmt_l');
    Route::post('/update/fct', 'ClientController@client_update_fct');
    Route::post('/update/nfct', 'ClientController@client_update_nfct');
    Route::post('/update/d_d_c', 'ClientController@client_update_d_d_c');
    Route::post('/update/d_c', 'ClientController@client_update_d_c');
    Route::get('/delete/{id}', 'ClientController@client_destroy');
    Route::get('/save-pdf/{id}', 'ClientController@client_pdf');
    Route::get('/save-xls/{id}', 'ClientController@client_xls');
});

Route::prefix('user')->group(function(){
    Route::get('/view', 'HomeController@users_view')->name('users.view');
    Route::get('/view-single/{id}', 'HomeController@user_view')->name('single.user.view');
    Route::post('/edit-photo', 'HomeController@user_edit_photo')->name('user.edit.photo');
    Route::post('/add-new', 'HomeController@add_new_user')->name('users.add');
    Route::get('/delete/{id}', 'HomeController@user_destroy')->name('users.delete');
    Route::get('/edit/{id}', 'HomeController@get_single_user');
    Route::post('/edit', 'HomeController@edit_user')->name('users.edit');
});

Route::prefix('admin')->group(function(){
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/login', 'Admin\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Admin\AdminLoginController@login');
    Route::get('/logout', 'Admin\AdminLoginController@logout')->name('admin.logout');
    // organization management
    Route::get('/organization', 'AdminController@organization')->name('admin.organization');
    Route::post('/organization/store', 'AdminController@organization_store')->name('admin.organization.store');
    Route::post('/organization/update', 'AdminController@organization_update')->name('admin.organization.update');
    Route::get('/organization/get/{id}', 'AdminController@organization_get');
    Route::get('/organization/delete/{id}', 'AdminController@organization_destroy');
    // users management
    Route::get('/users', 'AdminController@explore_users')->name('admin.users');
    Route::post('/user/store', 'AdminController@user_store')->name('admin.users.store');
    Route::post('/user/update', 'AdminController@user_update')->name('admin.users.update');
    Route::get('/user/get/{id}', 'AdminController@user_get');
    Route::get('/user/delete/{id}', 'AdminController@user_destroy');
    // clients management
    Route::get('/clients', 'AdminController@explore_clients')->name('admin.clients');
    Route::post('/table-setting', 'AdminController@custome_table')->name('admin.client.tablesetting');
    Route::post('/save-pdf', 'AdminController@client_pdf')->name('admin.client.pdf');
    Route::post('/save-xls', 'AdminController@client_xls')->name('admin.client.xls');
});

Route::get('login', function(){
    return redirect()->route('organization');
});
Route::get('organization', 'Auth\LoginController@index')->name('organization');
Route::post('login-org', 'Auth\LoginController@showLoginForm')->name('login.org');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('register-org', 'Auth\RegisterController@showRegistrationForm')->name('register.org');
Route::post('register', 'Auth\RegisterController@register')->name('register');
Route::get('password-reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password-email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password-reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password-reset', 'Auth\ResetPasswordController@reset');
