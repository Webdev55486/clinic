<?php

use App\ClientSettingAdmin;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ClientSettingAdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('client_setting_admin')->delete();

        $settings = [
            [
                'age' => 1,
                'gender' => 1,
                'dob' => 1,
                'doa' => 1,
                'ie' => 1,
                'ddv' => 0,
                'ndv' => 0,
                're' => 0,
                'nre' => 0,
                'ref' => 0,
                'nref' => 0,
                'rom' => 0,
                'nrom' => 0,
                'mmt_u' => 0,
                'nmmt_u' => 0,
                'mmt_l' => 0,
                'nmmt_l' => 0,
                'fct' => 0,
                'nfct' => 0,
                'd_d_c' => 0,
                'd_c' => 0,
                'width' => 1,
            ]
        ];

        foreach ($settings as $setting) {
            ClientSettingAdmin::create($setting);
        }
        Model::reguard();
    }
}
