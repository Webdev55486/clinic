<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('organization_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('gender');
            $table->integer('age');
            $table->date('dob')->nullable();
            $table->date('doa')->nullable();
            $table->date('ie')->nullable();
            $table->date('ddv')->nullable();
            $table->date('ndv')->nullable();
            $table->date('re')->nullable();
            $table->date('nre')->nullable();
            $table->date('ref')->nullable();
            $table->date('nref')->nullable();
            $table->date('rom')->nullable();
            $table->date('nrom')->nullable();
            $table->date('mmt_u')->nullable();
            $table->date('nmmt_u')->nullable();
            $table->date('mmt_l')->nullable();
            $table->date('nmmt_l')->nullable();
            $table->date('fct')->nullable();
            $table->date('nfct')->nullable();
            $table->date('d_d_c')->nullable();
            $table->date('d_c')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
