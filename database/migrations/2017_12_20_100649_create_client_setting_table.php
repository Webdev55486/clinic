<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->boolean('age')->default(true);
            $table->boolean('gender')->default(true);
            $table->boolean('dob')->default(true);
            $table->boolean('doa')->default(true);
            $table->boolean('ie')->default(true);
            $table->boolean('ddv')->default(true);
            $table->boolean('ndv')->default(true);
            $table->boolean('re')->default(true);
            $table->boolean('nre')->default(true);
            $table->boolean('ref')->default(true);
            $table->boolean('nref')->default(true);
            $table->boolean('rom')->default(true);
            $table->boolean('nrom')->default(true);
            $table->boolean('mmt_u')->default(true);
            $table->boolean('nmmt_u')->default(true);
            $table->boolean('mmt_l')->default(true);
            $table->boolean('nmmt_l')->default(true);
            $table->boolean('fct')->default(true);
            $table->boolean('nfct')->default(true);
            $table->boolean('d_d_c')->default(true);
            $table->boolean('d_c')->default(true);
            $table->integer('width')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_setting');
    }
}
