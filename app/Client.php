<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'clients';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'first_name', 'last_name', 'age', 'gender', 'dob', 'doa', 'ie','ddv', 'ndv', 're', 'nre', 'ref', 'nref', 'rom', 'nrom', 'mmt_u', 'nmmt_u', 'mmt_l', 'nmmt_l',
         'fct', 'nfct', 'd_d_c', 'd_c',
    ];

    public $timestamps = false;
}
