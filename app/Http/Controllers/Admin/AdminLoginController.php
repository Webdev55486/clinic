<?php

namespace App\Http\Controllers\Admin;

use Auth;
use \App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminLoginController extends Controller
{
     public function __construct()
     {
         $this->middleware('guest:admin')->except('logout');
     }
    
     public function showLoginForm()
     {
         return view('auth.admin_login');
     }
 
     public function validateLogin($request)
     {
         $this->validate($request, [
             'name' => 'required', 'password' => 'required'
         ]);
     }
 
     protected function credentials(Request $request)
     {
         return array_merge($request->only('name', 'password'));
     }
 
     public function login(Request $request)
     {
 
         $this->validateLogin($request);
 
         $remember = $request->input('remember');
         $getAdmininfo = $this->credentials($request);
 
         $availablecheck = Auth::guard('admin')->attempt($getAdmininfo, $remember);
 
         if ($availablecheck) {
             return redirect()->route('admin.organization');
         }
         return redirect()->back()->withInput($request->only('name','remember'))->with('error', 'These credentials do not match.');
     }
 
     public function logout()
     {
         Auth::guard('admin')->logout();
 
         return redirect('/admin');
     }
}
