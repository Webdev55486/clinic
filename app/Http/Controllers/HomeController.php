<?php

namespace App\Http\Controllers;

use Auth;
use DateTime;
use App\User;
use App\Slim;
use App\Client;
use App\ClientSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('home');
    }

    public function profile() {
        return view('profile');
    }

    public function profile_edit_bio(Request $request) {
        $user = Auth::user();

        $old_bio = $user->first_name.$user->last_name.$user->username;
        $new_bio = $request->first_name.$request->last_name.$request->username;

        if ($old_bio == $new_bio) {
            return back();
        }
        else {
            $update_result = "";

            if ($user->first_name != $request->first_name) {
                $user->first_name = $request->first_name;
                $user->save();
                $update_result = "First Name";
            }

            if ($user->last_name != $request->last_name) {
                $user->last_name = $request->last_name;
                $user->save();
                if ($update_result == "") {
                    $update_result = "Last Name";
                }
                else {
                    $update_result = $update_result.", Last Name";
                }
            }

            if ($user->username != $request->username) {
                if($this->isExistUsername($request->username) == true) {
                    return back()->with('error', 'This Username ('.$request->username.') has already been taken.');
                }
                else {
                    $user->username = $request->username;
                    $user->save();
                    if ($update_result == "") {
                        $update_result = "User Name";
                    }
                    else {
                        $update_result = $update_result.", User Name";
                    }
                }
            }

            return back()->with('status',  $update_result." Updated Successfully !");
        }
    }

    public function isExistUsername($username) {
        $user = User::where(array('username' => $username))->count();
        if($user != 0){
            return true;
        }else{
            return false;
        }
    }

    public function profile_edit_photo(Request $request) {

        if (Auth::check()) {
            $imageRand = rand(1000, 9999);
            $user = Auth::user();
            $random_name = $user->id."_".$imageRand;
            $dst = public_path('assets/images/avatar/');
            
            $rules = [
                'file' => 'image',
                'slim[]' => 'image'
                ];
            
            $validator = Validator::make($request->all(), $rules);
            $errors = $validator->errors();

            if($validator->fails()){
                return back()->with('error', 'Slim was not used to upload these imageser');
            }
            // dd($request->all());
            
            // Get posted data
            $images = Slim::getImages();
            // No image found under the supplied input name

            if ($images == false) {
                return back()->with('error', 'Slim was not used to upload these images');
            }
            else {
                foreach ($images as $image) {

                    $files = array();

                    // save output data if set
                    if (isset($image['output']['data'])) {

                        // Save the file
                        $origine_name = $image['output']['name'];
                        $file_type = pathinfo($origine_name, PATHINFO_EXTENSION);
                        $name = $random_name.".".$file_type;

                        // We'll use the output crop data
                        $data = $image['output']['data'];
                        
                        $output = Slim::saveFile($data, $name, $dst, false);
                        
                        $user->avatar = $name;
                        $user->save();
                        array_push($files, $output);
                    }

                    // save input data if set
                    if (isset ($image['input']['data'])) {

                        // Save the file
                        $origine_name = $image['input']['name'];
                        $file_type = pathinfo($origine_name, PATHINFO_EXTENSION);
                        
                        $name = $random_name.".".$file_type;
                        
                        $data = $image['input']['data'];
                        $input = Slim::saveFile($data, $name, $dst, false);
                        
                        $user->avatar = $name;
                        $user->save();
                        array_push($files, $input);
                    }
                }
            }
            
            return back()->with('status', 'Your Photo changed successfully');
        } else {
            return back()->with('error', 'You have no access for this action');
        }
    }

    public function user_edit_photo(Request $request) {
        $user = User::find($request->user_id);
        if ($user) {
            $imageRand = rand(1000, 9999);
            $random_name = $user->id."_".$imageRand;
            $dst = public_path('assets/images/avatar/');
            
            $rules = [
                'file' => 'image',
                'slim[]' => 'image'
                ];
            
            $validator = Validator::make($request->all(), $rules);
            $errors = $validator->errors();

            if($validator->fails()){
                return back()->with('error', 'Slim was not used to upload these imageser');
            }
            // dd($request->all());
            
            // Get posted data
            $images = Slim::getImages();
            // No image found under the supplied input name

            if ($images == false) {
                return back()->with('error', 'Slim was not used to upload these images');
            }
            else {
                foreach ($images as $image) {

                    $files = array();

                    // save output data if set
                    if (isset($image['output']['data'])) {

                        // Save the file
                        $origine_name = $image['output']['name'];
                        $file_type = pathinfo($origine_name, PATHINFO_EXTENSION);
                        $name = $random_name.".".$file_type;

                        // We'll use the output crop data
                        $data = $image['output']['data'];
                        
                        $output = Slim::saveFile($data, $name, $dst, false);
                        
                        $user->avatar = $name;
                        $user->save();
                        array_push($files, $output);
                    }

                    // save input data if set
                    if (isset ($image['input']['data'])) {

                        // Save the file
                        $origine_name = $image['input']['name'];
                        $file_type = pathinfo($origine_name, PATHINFO_EXTENSION);
                        
                        $name = $random_name.".".$file_type;
                        
                        $data = $image['input']['data'];
                        $input = Slim::saveFile($data, $name, $dst, false);
                        
                        $user->avatar = $name;
                        $user->save();
                        array_push($files, $input);
                    }
                }
            }

            return back()->with('status', 'Your Photo changed successfully');
        } else {
            return back()->with('error', 'You have no access for this action');
        }
    }

    public function profile_edit_pass(Request $request) {
        if (Auth::check()) {
            $requestData = $request->All();

            $current_password = Auth::user()->password;
            if (Hash::Check($requestData['current_password'], $current_password)) {
                $obj_user = Auth::user();
                $obj_user->password = bcrypt($requestData['password']);
                $obj_user->save();
                return back()->with('status', 'Password changed successfully');
            } else {
                return back()->with('error', 'Please enter correct current password');
            }
        } else {
            return redirect()->to('/');
        }
    }

    public function users_view() {
        if (Auth::user()->role == 1) {
            $users = User::where('organization_id', Auth::user()->organization_id)->get();
            return view('userManage', ['users' => $users]);
        }else {
            return back();
        }
    }

    public function user_view($id) {
        $user = User::find($id);
        return view('userProfile', ['user' => $user]);
    }

    protected function create(array $data) {
        return User::create([
            'organization_id' => Auth::user()->organization_id,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'role' => 0,
        ]);
    }

    public function add_new_user(Request $request) {

        $data = $request->all();

        // Standard validation
        $validator = Validator::make($data, [
            'username' => 'unique:users',
        ]);

        if ($validator->fails()) {
            return back()->with('error', 'This Username has already been taken.');
        }
        else {
            event(new Registered($user = $this->create($request->all())));
            return back()->with('status', 'Created New user.');
        }
    }

    public function edit_user(Request $request) {
        $user = User::find($request->user_id);
        if ($user) {
            $user->first_name = $request->_first_name;
            $user->last_name = $request->_last_name;
            $user->username = $request->_username;
            $user->save();

            if ($request->_password) {
                $user->password = bcrypt($request->_password);
                $user->save();
            }

            return back()->with('status', 'User Edited Successfully');
        }

        return back()->with('error', 'Can not find this User');
    }

    public function user_destroy($id) {
        $user = User::find($id);
        if ($user) {
            $user->delete();
            return "success";
        }
        return "error";
    }

    public function get_single_user($id) {
        $user = User::find($id);
        if ($user) {
            return $user;
        }
        return "error";
    }
}
