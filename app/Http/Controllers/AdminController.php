<?php

namespace App\Http\Controllers;

use Auth;
use \App\Admin;
use \App\User;
use App\Client;
use DateTime;
use Excel;
use Knp\Snappy\Pdf;
use App\Organization;
use App\ClientSettingAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(){
        return view('admin.dashboard');
    }
    
    public function organization(){
        $organizations = Organization::all();
        return view('admin.organization', ['organizations' => $organizations]);
    }
    
    public function organization_store(Request $request){
        
        $organization = new Organization;
        $organization->name = $request->name;
        $organization->code = $request->code;
        $organization->description = $request->description;
        $organization->save();
        return back();
    }
    
    public function organization_update(Request $request){
        
        $organization = Organization::find($request->organization_id);
        if ($organization) {
            $organization->name = $request->_name;
            $organization->code = $request->_code;
            $organization->description = $request->_description;
            $organization->save();
            
            return back()->with('status', 'Organization Updated');
        }
        return back()->with('error', 'Can not found Organization');
    }
    
    public function organization_get($id){
        $organization = Organization::find($id);
        if ($organization) {
            return $organization;
        }
        return "fail";
    }
    
    public function organization_destroy($id){
        $organization = Organization::find($id);
        if ($organization) {
            $organization->delete();
            return "success";
        }
        return "fail";
    }
    
    // user Management
    
    public function explore_users(){
        $users = User::join('organizations', 'organizations.id' , '=', 'users.organization_id')->select('users.*', 'organizations.name as organization_name')->get();
        $organizations = Organization::all();
        return view('admin.users', ['users' => $users, 'organizations' => $organizations]);
    }
    
    public function user_store(Request $request) {
        
        $data = $request->all();

        // Standard validation
        $validator = Validator::make($data, [
            'username' => 'unique:users',
        ]);
        
        if ($validator->fails()) {
            return back()->with('error', 'This Username has already been taken.');
        }
        else {
            event(new Registered($user = $this->user_create($request->all())));
            return back()->with('status', 'Created New user.');
        }
    }
    
    public function user_update(Request $request) {
        $user = User::find($request->user_id);
        if ($user) {
            $user->username = $request->_username;
            $user->role = $request->_user_role;
            $user->organization_id = $request->_organization;
            $user->save();

            if ($request->_password) {
                $user->password = bcrypt($request->_password);
                $user->save();
            }

            return back()->with('status', 'User Edited Successfully');
        }

        return back()->with('error', 'Can not find this User');
    }
    
    protected function user_create(array $data) {
        return User::create([
            'organization_id' => $data['organization'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'role' => $data['user_role'],
        ]);
    }
    
    public function user_get($id) {
        $user = User::find($id);
        if ($user) {
            return $user;
        }
        return "fail";
    }
    
    public function user_destroy($id) {
        $user = User::find($id);
        if ($user) {
            $user->delete();
            return "success";
        }
        return "fail";
    }
    
    // Client Management
    
    public function explore_clients() {
        $clients = Client::join('organizations', 'organizations.id' , '=', 'clients.organization_id')->select('clients.*', 'organizations.name as organization_name')->get();
        $organizations = Organization::all();
        return view('admin.clients', ['clients' => $clients, 'organizations' => $organizations]);
    }
    
    public function custome_table(Request $request) {
        $table_cells = $request->table_setting;
        $table_setting = ClientSettingAdmin::first();
        $table_setting->age = 0;
        $table_setting->gender = 0;
        $table_setting->dob = 0;
        $table_setting->doa = 0;
        $table_setting->ie = 0;
        $table_setting->ddv = 0;
        $table_setting->ndv = 0;
        $table_setting->re = 0;
        $table_setting->nre = 0;
        $table_setting->ref = 0;
        $table_setting->nref = 0;
        $table_setting->rom = 0;
        $table_setting->nrom = 0;
        $table_setting->mmt_u = 0;
        $table_setting->nmmt_u = 0;
        $table_setting->mmt_l = 0;
        $table_setting->nmmt_l = 0;
        $table_setting->fct = 0;
        $table_setting->nfct = 0;
        $table_setting->d_d_c = 0;
        $table_setting->d_c = 0;
        $table_setting->save();
        
        if ($table_cells != "") {
            foreach($table_cells as $table_cell){
                $table_setting->$table_cell = 1;
            }
            $table_setting->save();
        }
        return back();
    }
    
    public function client_pdf(Request $request) {
        $clients = Client::where('organization_id', $request->organization_id)->get();
        // return view('clientpdf', ['clients' => $clients]);
        
        $pdf = \App::make('snappy.pdf.wrapper');
        $pdf->loadView('clientpdf', ['clients' => $clients]);
        $pdf->setPaper('a2');
        $pdf->setOrientation('landscape');
        $pdf->setOption('margin-bottom', 1);
        $pdf->setOption('margin-top', 20);
        $pdf->setOption('header-left', '[date]');
        $pdf->setOption('header-right', 'Page [page] of [topage]');
        $pdf->setOption('header-font-size', 10);
        // $pdf->setOption('page-width', '215.9');
        
        return $pdf->inline('clients.pdf');
    }
    
    public function change_dateformat($date) {
        if ($date) {
            $resultdate = DateTime::createFromFormat('Y-m-d', $date);
            $final_date = $resultdate->format('m/d/Y');
            return $final_date;
        }
        else {
            return "NULL";
        }
    }
    
    public function client_xls(Request $request) {
        $clients = Client::where('organization_id', $request->organization_id)->get();
        
        $clients_array = array();
        foreach ($clients as $client) {
            $client_gender = $client->gender == 1 ? 'Male' : 'Female';
            $clients_array[] = array(
                'First Name' => $client->first_name,
                'Last Name' => $client->last_name,
                'Gender' => $client_gender,
                'Age' => $client->age,
                'DOA' => $this->change_dateformat($client->doa),
                'DOB' => $this->change_dateformat($client->dob),
                'IE' => $this->change_dateformat($client->ie),
                'DDV' => $this->change_dateformat($client->ddv),
                'NDV' => $this->change_dateformat($client->ndv),
                'RE' => $this->change_dateformat($client->re),
                'NRE' => $this->change_dateformat($client->nre),
                'REF' => $this->change_dateformat($client->ref),
                'NREF' => $this->change_dateformat($client->nref),
                'ROM' => $this->change_dateformat($client->rom),
                'NROM' => $this->change_dateformat($client->nrom),
                'MMT-U' => $this->change_dateformat($client->mmt_u),
                'NMMT-U' => $this->change_dateformat($client->nmmt_u),
                'MMT-L' => $this->change_dateformat($client->mmt_l),
                'NMMT-L' => $this->change_dateformat($client->nmmt_l),
                'FCT' => $this->change_dateformat($client->fct),
                'NFCT' => $this->change_dateformat($client->nfct),
                'D-D/C' => $this->change_dateformat($client->d_d_c),
                'D/C' => $this->change_dateformat($client->d_c),
            );
        }
        return Excel::create('clients', function($excel) use ($clients_array) {
			$excel->sheet('mySheet', function($sheet) use ($clients_array)
	        {
				$sheet->fromArray($clients_array);
	        });
		})->export('csv');
    }
}
