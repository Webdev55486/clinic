<?php

namespace App\Http\Controllers\Auth;

use Auth;
use \App\User;
use App\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function index()
    {
        return view('auth.organization');
    }
    
    public function showLoginForm(Request $request)
    {
        $organization_count = Organization::where('code', $request->organication)->count();
        if ($organization_count > 0) {
            $organization = Organization::where('code', $request->organication)->first();
            return view('auth.login', ['organization' => $organization]);
        }
        
        return back()->with('error', 'code not match');
    }

    public function validateLogin($request)
    {
        $this->validate($request, [
            'organization_id' => 'required',
            'username' => 'required',
            'password' => 'required'
        ]);
    }

    protected function credentials(Request $request)
    {
        return array_merge($request->only('organization_id','username', 'password'));
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
        $remember = $request->input('remember');
        $getuserinfo = $this->credentials($request);
        $availablecheck = Auth::guard()->attempt($getuserinfo, $remember);

        if ($availablecheck) {
            $this->isRemember($request);
            return redirect()->route('client.view');
        }
        return redirect()->back()->with('error', 'Invalid Login – Please Try Again.');
    }

    public function isRemember($request){
        if($request->input('remember')){
            //for 30 days
            $time = time() + (86400 * 30);
            $this->cookieSet("username",$request->input('username'),$time);
            $this->cookieSet("password",$request->input('password'),$time);
        }
    }

   public function cookieSet($name, $value, $time)
   {
       setcookie($name, $value, $time, "/");
   }
}
