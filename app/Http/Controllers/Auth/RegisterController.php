<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
     
     public function showRegistrationForm(Request $request)
     {
         // dd($request->all());
         $organization = Organization::find($request->organization_id);
         
         if ($organization) {
             return view('auth.register', ['organization' => $organization]);
         }
         
         return back()->with('error', 'code not match');
     }
    
    protected function create(array $data)
    {
        return User::create([
            'organization_id' => $data['organization_id'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'role' => 0,
        ]);
    }

    public function register(Request $request)
    {
        $data = $request->all();

        // Standard validation
        $validator = Validator::make($data, [
            'username' => 'unique:users',
        ]);

        if ($validator->fails()) {
            return back()->with('error', 'This Username has already been taken.');
        }
        else {
            event(new Registered($user = $this->create($request->all())));

            $this->guard()->login($user);

            return redirect('/');
        }
    }
}
