<?php

namespace App\Http\Controllers;

use Auth;
use Excel;
use DateTime;
use App\User;
use App\Client;
use Knp\Snappy\Pdf;
use App\Organization;
use App\ClientSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth');
    }
    
    public function client() {
        $clients = Client::where('organization_id', Auth::user()->organization_id)->get();
        $organizations = Organization::all();
        return view('clientManage', ['clients' => $clients, 'organizations' => $organizations]);
    }
    
    public function client_pdf($id) {
        $clients = Client::where('organization_id', $id)->get();
        // return view('clientpdf', ['clients' => $clients]);
        
        $pdf = \App::make('snappy.pdf.wrapper');
        $pdf->loadView('clientpdf', ['clients' => $clients]);
        $pdf->setPaper('a2');
        $pdf->setOrientation('landscape');
        $pdf->setOption('margin-bottom', 1);
        $pdf->setOption('margin-top', 20);
        $pdf->setOption('header-left', '[date]');
        $pdf->setOption('header-right', 'Page [page] of [topage]');
        $pdf->setOption('header-font-size', 10);
        // $pdf->setOption('page-width', '215.9');
        
        return $pdf->inline('clients.pdf');
    }
    
    public function change_dateformat($date) {
        if ($date) {
            $resultdate = DateTime::createFromFormat('Y-m-d', $date);
            $final_date = $resultdate->format('m/d/Y');
            return $final_date;
        }
        else {
            return "NULL";
        }
    }
    
    public function client_xls($id) {
        $clients = Client::where('organization_id', Auth::user()->organization_id)->get();
        
        $clients_array = array();
        foreach ($clients as $client) {
            $client_gender = $client->gender == 1 ? 'Male' : 'Female';
            $clients_array[] = array(
                'First Name' => $client->first_name,
                'Last Name' => $client->last_name,
                'Gender' => $client_gender,
                'Age' => $client->age,
                'DOA' => $this->change_dateformat($client->doa),
                'DOB' => $this->change_dateformat($client->dob),
                'IE' => $this->change_dateformat($client->ie),
                'DDV' => $this->change_dateformat($client->ddv),
                'NDV' => $this->change_dateformat($client->ndv),
                'RE' => $this->change_dateformat($client->re),
                'NRE' => $this->change_dateformat($client->nre),
                'REF' => $this->change_dateformat($client->ref),
                'NREF' => $this->change_dateformat($client->nref),
                'ROM' => $this->change_dateformat($client->rom),
                'NROM' => $this->change_dateformat($client->nrom),
                'MMT-U' => $this->change_dateformat($client->mmt_u),
                'NMMT-U' => $this->change_dateformat($client->nmmt_u),
                'MMT-L' => $this->change_dateformat($client->mmt_l),
                'NMMT-L' => $this->change_dateformat($client->nmmt_l),
                'FCT' => $this->change_dateformat($client->fct),
                'NFCT' => $this->change_dateformat($client->nfct),
                'D-D/C' => $this->change_dateformat($client->d_d_c),
                'D/C' => $this->change_dateformat($client->d_c),
            );
        }
        return Excel::create('clients', function($excel) use ($clients_array) {
			$excel->sheet('mySheet', function($sheet) use ($clients_array)
	        {
				$sheet->fromArray($clients_array);
	        });
		})->export('csv');
    }
    
    public function custome_table(Request $request) {
        $table_cells = $request->table_setting;
        $table_setting = ClientSetting::where('user_id', Auth::user()->id)->first();
        if ($table_setting) {
            $table_setting->age = 0;
            $table_setting->gender = 0;
            $table_setting->dob = 0;
            $table_setting->doa = 0;
            $table_setting->ie = 0;
            $table_setting->ddv = 0;
            $table_setting->ndv = 0;
            $table_setting->re = 0;
            $table_setting->nre = 0;
            $table_setting->ref = 0;
            $table_setting->nref = 0;
            $table_setting->rom = 0;
            $table_setting->nrom = 0;
            $table_setting->mmt_u = 0;
            $table_setting->nmmt_u = 0;
            $table_setting->mmt_l = 0;
            $table_setting->nmmt_l = 0;
            $table_setting->fct = 0;
            $table_setting->nfct = 0;
            $table_setting->d_d_c = 0;
            $table_setting->d_c = 0;
            $table_setting->save();
        }
        else {
            $table_setting = new ClientSetting;
            $table_setting->user_id = Auth::user()->id;
            $table_setting->age = 0;
            $table_setting->gender = 0;
            $table_setting->dob = 0;
            $table_setting->doa = 0;
            $table_setting->ie = 0;
            $table_setting->ddv = 0;
            $table_setting->ndv = 0;
            $table_setting->re = 0;
            $table_setting->nre = 0;
            $table_setting->ref = 0;
            $table_setting->nref = 0;
            $table_setting->rom = 0;
            $table_setting->nrom = 0;
            $table_setting->mmt_u = 0;
            $table_setting->nmmt_u = 0;
            $table_setting->mmt_l = 0;
            $table_setting->nmmt_l = 0;
            $table_setting->fct = 0;
            $table_setting->nfct = 0;
            $table_setting->d_d_c = 0;
            $table_setting->d_c = 0;
            $table_setting->save();
        }
        
        if ($table_cells != "") {
            foreach($table_cells as $table_cell){
                $table_setting->$table_cell = 1;
            }
            $table_setting->save();
        }
        return back();
    }
    
    public function client_store(Request $request) {
        $client = new Client();

        $username = $request->first_name." ".$request->last_name;

        if($this->check_client_name($username) == true) {
            return back()->with('error', 'This Name ('.$username.') has already been taken.');
        }else {
            $result_doa = DateTime::createFromFormat('m/d/Y', $request->doa);
            $date_doa = $result_doa->format('Y-m-d');

            $result_dob = DateTime::createFromFormat('m/d/Y', $request->dob);
            $date_dob = $result_dob->format('Y-m-d');

            $result_ie = DateTime::createFromFormat('m/d/Y', $request->ie);
            $date_ie = $result_ie->format('Y-m-d');

            $result_ref = DateTime::createFromFormat('m/d/Y', $request->ref);
            $date_ref = $result_ref->format('Y-m-d');

            $client->organization_id = $request->organization;
            $client->first_name = $request->first_name;
            $client->last_name = $request->last_name;
            $client->doa = $date_doa;
            $client->dob = $date_dob;
            $client->age = $request->age;
            $client->gender = $request->gender;
            $client->ie = $date_ie;
            $client->ref = $date_ref;
            $client->save();
            return back()->with('status', 'New Client ('.$username.') added successfully.');
        }
    }
    
    protected function check_client_name($username) {
        $count = 0;
        $clients = Client::all();
        foreach ($clients as $client) {
            $client_name = $client->first_name." ".$client->last_name;
            if ($client_name == $username) {
                $count += 1;
            }
        }

        if ($count != 0) {
            return true;
        }else {
            return false;
        }
    }
    
    public function client_update(Request $request) {
        $client = Client::find($request->client_id);
        $client_cells = $request->client_info;
        $result = DateTime::createFromFormat('m/d/Y', $request->today);
        $final_date = $result->format('Y-m-d');

        if ($client && $client_cells != "") {

            $username = $client->first_name." ".$client->last_name;

            foreach ($client_cells as $client_cell) {
                $client->$client_cell = $final_date;
            }

            $client->save();

            return back()->with('status', 'Client ('.$username.') added Info successfully.');

        }else {
            return back()->with('error', 'Something went Wrong.');
        }
    }
    
    public function get_setting_width($width) {
        $setting = ClientSetting::first();
        $setting->width = $width;
        $setting->save();
        return "success";
    }
    
    public function get_single_client($id) {
        $client = Client::find($id);
        if ($client) {
            return $client;
        }else {
            return "error";
        }
    }
    
    public function client_destroy($id) {
        $client = Client::find($id);
        if ($client) {
            $client->delete();
            return "success";
        }else {
            return "error";
        }
    }
    
    public function client_update_dob(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->dob = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_doa(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->doa = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_ie(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->ie = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_ddv(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->ddv = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_ndv(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->ndv = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_re(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->re = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_nre(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->nre = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_ref(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->ref = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_nref(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->nref = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_rom(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->rom = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_nrom(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->nrom = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_mmt_u(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->mmt_u = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_nmmt_u(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->nmmt_u = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_mmt_l(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->mmt_l = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_nmmt_l(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->nmmt_l = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_fct(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->fct = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_nfct(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->nfct = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_d_d_c(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->d_d_c = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function client_update_d_c(Request $request) {
        $client = Client::find($request->clientId);
        if ($client) {
            $date = $this->change_format($request->currentDate);
            $client->d_c = $date;
            $client->save();
            return "success";
        }
        return "fail";
    }
    
    public function change_format($date) {
        $result = DateTime::createFromFormat('m/d/Y', $date);
        $result_date = $result->format('Y-m-d');
        return $result_date;
    }
}
