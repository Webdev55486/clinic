<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientSetting extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'client_setting';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'age', 'gender', 'dob', 'doa', 'ie','ddv', 'ndv', 're', 'nre', 'ref', 'nref', 'rom', 'nrom', 'mmt_u', 'nmmt_u', 'mmt_l', 'nmmt_l', 'fct', 'nfct', 'd_d_c',
        'd_c', 'width',
    ];

    public $timestamps = false;
}
