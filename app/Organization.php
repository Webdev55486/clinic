<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'organizations';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'name', 'code', 'description'
    ];

    public $timestamps = false;
}
